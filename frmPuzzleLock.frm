VERSION 5.00
Begin VB.Form frmPuzzleLock 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Puzzle Lock Simulator"
   ClientHeight    =   3510
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4995
   Icon            =   "frmPuzzleLock.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3510
   ScaleWidth      =   4995
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrClock 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   3960
      Top             =   2880
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      Height          =   375
      Left            =   3240
      TabIndex        =   37
      Top             =   1800
      Width           =   1695
   End
   Begin VB.CommandButton cmdPause 
      Cancel          =   -1  'True
      Caption         =   "Pause"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3240
      TabIndex        =   36
      Top             =   1320
      Width           =   1695
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      Default         =   -1  'True
      Height          =   375
      Left            =   3240
      TabIndex        =   34
      Top             =   840
      Width           =   1695
   End
   Begin VB.Frame fraC 
      Caption         =   "C"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   0
      TabIndex        =   29
      Top             =   2160
      Width           =   1215
      Begin VB.OptionButton optC 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   840
         TabIndex        =   32
         Top             =   240
         Width           =   255
      End
      Begin VB.OptionButton optC 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   31
         Top             =   600
         Value           =   -1  'True
         Width           =   255
      End
      Begin VB.OptionButton optC 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   30
         Top             =   960
         Width           =   255
      End
   End
   Begin VB.Frame fraM 
      Caption         =   "M"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   1920
      TabIndex        =   25
      Top             =   2160
      Width           =   1215
      Begin VB.OptionButton optM 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   255
      End
      Begin VB.OptionButton optM 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   27
         Top             =   600
         Value           =   -1  'True
         Width           =   255
      End
      Begin VB.OptionButton optM 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   840
         TabIndex        =   26
         Top             =   960
         Width           =   255
      End
   End
   Begin VB.Frame fraN 
      Caption         =   "N"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   1920
      TabIndex        =   21
      Top             =   0
      Width           =   1215
      Begin VB.OptionButton optN 
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   24
         Top             =   960
         Width           =   255
      End
      Begin VB.OptionButton optN 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   23
         Top             =   600
         Value           =   -1  'True
         Width           =   255
      End
      Begin VB.OptionButton optN 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   840
         TabIndex        =   22
         Top             =   240
         Width           =   255
      End
   End
   Begin VB.Frame fraE 
      Caption         =   "E"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   1215
      Begin VB.OptionButton optE 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   840
         TabIndex        =   20
         Top             =   960
         Width           =   255
      End
      Begin VB.OptionButton optE 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   19
         Top             =   600
         Value           =   -1  'True
         Width           =   255
      End
      Begin VB.OptionButton optE 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   255
      End
   End
   Begin VB.Frame fraL 
      Caption         =   "L"
      Enabled         =   0   'False
      Height          =   615
      Left            =   1920
      TabIndex        =   13
      Top             =   1440
      Width           =   1215
      Begin VB.OptionButton optL 
         Caption         =   "Option4"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   255
      End
      Begin VB.OptionButton optL 
         Caption         =   "Option5"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   840
         TabIndex        =   15
         Top             =   240
         Width           =   255
      End
      Begin VB.OptionButton optL 
         Caption         =   "Option6"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   14
         Top             =   240
         Value           =   -1  'True
         Width           =   255
      End
   End
   Begin VB.Frame fraO 
      Caption         =   "O"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   1320
      TabIndex        =   9
      Top             =   2160
      Width           =   495
      Begin VB.OptionButton optO 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   255
      End
      Begin VB.OptionButton optO 
         Caption         =   "Option2"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   600
         Value           =   -1  'True
         Width           =   255
      End
      Begin VB.OptionButton optO 
         Caption         =   "Option3"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   960
         Width           =   255
      End
   End
   Begin VB.Frame fraI 
      Caption         =   "I"
      Enabled         =   0   'False
      Height          =   615
      Left            =   0
      TabIndex        =   5
      Top             =   1440
      Width           =   1215
      Begin VB.OptionButton optI 
         Caption         =   "Option6"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   8
         Top             =   240
         Value           =   -1  'True
         Width           =   255
      End
      Begin VB.OptionButton optI 
         Caption         =   "Option5"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   840
         TabIndex        =   7
         Top             =   240
         Width           =   255
      End
      Begin VB.OptionButton optI 
         Caption         =   "Option4"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   255
      End
   End
   Begin VB.Frame fraK 
      Caption         =   "K"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   1320
      TabIndex        =   1
      Top             =   0
      Width           =   495
      Begin VB.OptionButton optK 
         Caption         =   "Option3"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   4
         Top             =   960
         Width           =   255
      End
      Begin VB.OptionButton optK 
         Caption         =   "Option2"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   3
         Top             =   600
         Value           =   -1  'True
         Width           =   255
      End
      Begin VB.OptionButton optK 
         Caption         =   "Option1"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   255
      End
   End
   Begin VB.CommandButton cmdGo 
      Caption         =   "Go"
      Enabled         =   0   'False
      Height          =   615
      Left            =   1320
      TabIndex        =   0
      Top             =   1440
      Width           =   495
   End
   Begin VB.Label lblClock 
      Alignment       =   2  'Center
      Caption         =   "Clock"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3240
      TabIndex        =   35
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label lblTime 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0:00:00"
      Height          =   255
      Left            =   3240
      TabIndex        =   33
      Top             =   480
      Width           =   1695
   End
End
Attribute VB_Name = "frmPuzzleLock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public started As Single, elapsed As Single

Private Sub Form_Load()
  started = 0
  elapsed = 0
End Sub

Private Sub cmdStart_Click()
  Enable_Disable True
  cmdStart.Enabled = False
  tmrClock.Enabled = True
  If started = 0 Then started = Timer
End Sub

Private Sub Enable_Disable(newval As Boolean)
  Dim i As Integer
  fraE.Enabled = newval
  fraK.Enabled = newval
  fraN.Enabled = newval
  fraL.Enabled = newval
  fraM.Enabled = newval
  fraO.Enabled = newval
  fraC.Enabled = newval
  fraI.Enabled = newval
  For i = 0 To 2
    optE(i).Enabled = newval
    optK(i).Enabled = newval
    optN(i).Enabled = newval
    optL(i).Enabled = newval
    optM(i).Enabled = newval
    optO(i).Enabled = newval
    optC(i).Enabled = newval
    optI(i).Enabled = newval
    Next i
  cmdGo.Enabled = newval
  cmdPause.Enabled = newval
End Sub

Private Sub Reset_Wedges(slowly As Boolean)
  optK(1).Value = True
  If slowly Then Sleep 600
  optC(1).Value = True
  If slowly Then Sleep 600
  optE(1).Value = True
  If slowly Then Sleep 600
  optM(1).Value = True
  If slowly Then Sleep 600
  optN(1).Value = True
  If slowly Then Sleep 600
  optI(1).Value = True
  If slowly Then Sleep 600
  optO(1).Value = True
  If slowly Then Sleep 600
  optL(1).Value = True
  If slowly Then Sleep 600
End Sub

Private Sub Sleep(ms As Integer)
  Dim t As Single
  t = Timer
  While Timer < t + (ms / 1000)
    DoEvents
    Wend
End Sub

Private Sub tmrClock_Timer()
  Dim seconds As Integer, minutes As Integer, hours As Integer
  seconds = elapsed + Timer - started
  minutes = 0
  hours = 0
  If seconds >= 60 Then
      minutes = Round(seconds / 60)
      seconds = seconds - 60 * minutes
    End If
  If minutes >= 60 Then
      hours = Round(minutes / 60)
      minutes = minutes - 60 * hours
    End If
  lblTime = CStr(hours) & ":" & Right("00" & CStr(minutes), 2) & ":" & Right("00" & CStr(seconds), 2)
End Sub

Private Sub cmdPause_Click()
  tmrClock_Timer
  elapsed = elapsed + Timer - started
  started = 0
  Enable_Disable False
  cmdStart.Caption = "Resume"
  cmdStart.Enabled = True
  tmrClock.Enabled = False
End Sub

Private Sub cmdReset_Click()
  started = 0
  elapsed = 0
  lblTime = "0:00:00"
  Enable_Disable False
  Reset_Wedges False
  cmdStart.Caption = "Start"
  cmdStart.Enabled = True
  tmrClock.Enabled = False
End Sub

Private Sub cmdGo_Click()
  If optL(0) And optO(0) And optC(0) And optK(0) And _
     optM(1) And optE(1) And _
     optI(2) And optN(2) Then
      tmrClock.Enabled = False
      MsgBox "Congratulations!  (Total time: " & lblTime & ")", vbOKOnly, "Puzzle Lock Simulator"
      cmdReset_Click
      Exit Sub
    End If
  frmPuzzleLock.Caption = "Puzzle Lock Simulator - Resetting, please wait..."
  Enable_Disable False
  Reset_Wedges True
  Enable_Disable True
  frmPuzzleLock.Caption = "Puzzle Lock Simulator"
End Sub


